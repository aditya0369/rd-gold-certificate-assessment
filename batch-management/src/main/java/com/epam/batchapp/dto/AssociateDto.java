package com.epam.batchapp.dto;

import com.epam.batchapp.model.Batch;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AssociateDto {

	private int id;

	@NotBlank(message = "name cannot be blank")
	private String name;

	@Email(message = "Invalid Email")
	private String email;

	@Pattern(regexp = "^(M|F)$", message = "Gender can only be 'M' or 'F'")
	private String gender;

	@NotBlank(message = "college cannot be blank")
	private String college;

	@Pattern(regexp = "^(ACTIVE|INACTIVE)$", message = "Status can only be 'ACTIVE' or 'INACTIVE'")
	private String status;

	private Batch batch;

}
