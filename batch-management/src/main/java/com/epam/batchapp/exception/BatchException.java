package com.epam.batchapp.exception;

public class BatchException extends RuntimeException{

	private static final long serialVersionUID = 1L;

	public BatchException (String message) {
		super(message);
	}
}
