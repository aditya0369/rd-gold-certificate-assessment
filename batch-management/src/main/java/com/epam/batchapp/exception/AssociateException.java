package com.epam.batchapp.exception;

public class AssociateException extends RuntimeException{

	private static final long serialVersionUID = 1L;

	public AssociateException(String msg) {
		super(msg);
	}

}
