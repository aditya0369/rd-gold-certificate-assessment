package com.epam.batchapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.epam.batchapp.model.Batch;

public interface BatchRepository extends JpaRepository<Batch, Integer>{

}
