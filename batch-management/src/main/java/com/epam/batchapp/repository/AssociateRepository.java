package com.epam.batchapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.epam.batchapp.model.Associate;

public interface AssociateRepository extends JpaRepository<Associate, Integer>{

	boolean existsByName(String name);
	List<Associate> findByGender(String gender);	
}
