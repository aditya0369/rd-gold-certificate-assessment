package com.epam.batchapp.service;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.batchapp.dto.AssociateDto;
import com.epam.batchapp.exception.AssociateException;
import com.epam.batchapp.exception.BatchException;
import com.epam.batchapp.model.Associate;
import com.epam.batchapp.repository.AssociateRepository;
import com.epam.batchapp.repository.BatchRepository;

@Service
public class AssociateService {

	@Autowired
	ModelMapper modelMapper;

	@Autowired
	AssociateRepository associateRepository;

	@Autowired
	BatchRepository batchRepository;

	public AssociateDto add(AssociateDto associateDto) {
		if (associateRepository.existsByName(associateDto.getName())) {
			throw new AssociateException("Associate with same name already exists");
		}
		if (!batchRepository.existsById(associateDto.getBatch().getId())) {
			throw new BatchException("batch not found");
		}
		Associate associate = modelMapper.map(associateDto, Associate.class);
		return modelMapper.map(associateRepository.save(associate), AssociateDto.class);
	}

	public void delete(int id) {
		associateRepository.deleteById(id);
	}

	public AssociateDto update(AssociateDto associateDto) {
		Associate associate = associateRepository.findById(associateDto.getId())
				.orElseThrow(() -> new AssociateException("no associate exists with this id"));
		modifyAssociate(associateDto, associate);
		return modelMapper.map(associateRepository.save(associate), AssociateDto.class);
	}

	public List<AssociateDto> getByGender(String gender) {
		if (!(gender.equals("M") || gender.equals("F"))) {
			throw new AssociateException("Please enter M for male and F for female");
		}
		return associateRepository.findByGender(gender).stream().map(a -> {
			AssociateDto associatesDto = modelMapper.map(a, AssociateDto.class);
			associatesDto.setBatch(a.getBatch());
			return associatesDto;
		}).toList();

	}

	private void modifyAssociate(AssociateDto associateDto, Associate associate) {
		associate.setBatch(associateDto.getBatch());
		associate.setCollege(associateDto.getCollege());
		associate.setEmail(associateDto.getEmail());
		associate.setGender(associateDto.getGender());
		associate.setName(associateDto.getName());
		associate.setStatus(associateDto.getStatus());
	}
}
