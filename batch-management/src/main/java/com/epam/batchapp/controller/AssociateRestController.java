package com.epam.batchapp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.epam.batchapp.dto.AssociateDto;
import com.epam.batchapp.service.AssociateService;

import jakarta.validation.Valid;

@RestController
@RequestMapping("rd/associates")
public class AssociateRestController {

	@Autowired
	AssociateService associateService;

	@PostMapping
	public ResponseEntity<AssociateDto> add(@RequestBody @Valid AssociateDto associateDTO) {
		return new ResponseEntity<>(associateService.add(associateDTO), HttpStatus.CREATED);
	}

	@PutMapping
	public ResponseEntity<AssociateDto> update(@RequestBody @Valid AssociateDto associateDto) {
		return new ResponseEntity<>(associateService.update(associateDto), HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Void> deleteAssociate(@PathVariable("id") int id) {
		associateService.delete(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@GetMapping("/{gender}")
	public ResponseEntity<List<AssociateDto>> getByGender(@PathVariable String gender) {
		return ResponseEntity.ok(associateService.getByGender(gender));
	}

}
