package com.epam.batchapp;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import com.epam.batchapp.dto.AssociateDto;
import com.epam.batchapp.exception.AssociateException;
import com.epam.batchapp.exception.BatchException;
import com.epam.batchapp.model.Associate;
import com.epam.batchapp.model.Batch;
import com.epam.batchapp.repository.AssociateRepository;
import com.epam.batchapp.repository.BatchRepository;
import com.epam.batchapp.service.AssociateService;

@ExtendWith(MockitoExtension.class)
public class ServiceTest {

	@Mock
	ModelMapper modelMapper;

	@Mock
	AssociateRepository associateRepository;

	@Mock
	private BatchRepository batchRepository;

	@InjectMocks
	AssociateService associateService;

	AssociateDto associateDto;
	Associate associate;
	Batch batch;

	@BeforeEach
	void setUp() {
		associateDto = AssociateDto.builder().name("aditya").email("aditya@email.com").college("Chitkara").build();
		associate = new Associate();
		associate.setName("aditya");
		associate.setEmail("aditya@gmail.com");
		associate.setCollege("Chitkara");
		batch = new Batch();
		batch.setId(1);
		associateDto.setBatch(batch);
	}

	@Test
	void testAddAssociate() {
		when(associateRepository.existsByName(associateDto.getName())).thenReturn(false);
        when(batchRepository.existsById(associateDto.getBatch().getId())).thenReturn(true);
        when(modelMapper.map(associateDto, Associate.class)).thenReturn(associate);
        when(associateRepository.save(associate)).thenReturn(associate);
        when(modelMapper.map(associate, AssociateDto.class)).thenReturn(associateDto);
        AssociateDto result = associateService.add(associateDto);
        assertEquals(associateDto, result);
        verify(associateRepository, Mockito.times(1)).existsByName(associateDto.getName());
        verify(batchRepository, Mockito.times(1)).existsById(associateDto.getBatch().getId());
        verify(associateRepository, Mockito.times(1)).save(associate);
	}

	@Test
	void testAddAssociate_AssociateWithSameNameExists_ExceptionThrown() {
		AssociateDto associateDto = new AssociateDto();
		associateDto.setName("John Doe");
		Mockito.when(associateRepository.existsByName(associateDto.getName())).thenReturn(true);
		Assertions.assertThrows(AssociateException.class, () -> associateService.add(associateDto));
		Mockito.verify(associateRepository, Mockito.times(1)).existsByName(associateDto.getName());
		Mockito.verify(batchRepository, Mockito.never()).existsById(Mockito.anyInt());
		Mockito.verify(associateRepository, Mockito.never()).save(Mockito.any());
	}

	@Test
	void testAddAssociate_BatchNotFound_ExceptionThrown() {
		AssociateDto associateDto = new AssociateDto();
		Batch batch = new Batch();
		batch.setId(1);
		associateDto.setBatch(batch);
		Mockito.when(associateRepository.existsByName(associateDto.getName())).thenReturn(false);
		Mockito.when(batchRepository.existsById(associateDto.getBatch().getId())).thenReturn(false);
		Assertions.assertThrows(BatchException.class, () -> associateService.add(associateDto));
		Mockito.verify(associateRepository, Mockito.times(1)).existsByName(associateDto.getName());
		Mockito.verify(batchRepository, Mockito.times(1)).existsById(associateDto.getBatch().getId());
		Mockito.verify(associateRepository, Mockito.never()).save(Mockito.any());
	}

	@Test
	void testDeleteAssociate_Success() {
		int associateId = 1;
		associateService.delete(associateId);
		Mockito.verify(associateRepository, Mockito.times(1)).deleteById(associateId);
	}

	@Test
	void testUpdateAssociate_Success() {
		Mockito.when(associateRepository.findById(associateDto.getId())).thenReturn(Optional.of(associate));
		Mockito.when(associateRepository.save(associate)).thenReturn(associate);
		Mockito.when(modelMapper.map(associate, AssociateDto.class)).thenReturn(associateDto);
		AssociateDto result = associateService.update(associateDto);
		Assertions.assertEquals(associateDto, result);
		Mockito.verify(associateRepository, Mockito.times(1)).findById(associateDto.getId());
		Mockito.verify(associateRepository, Mockito.times(1)).save(associate);
	}

	@Test
	void testUpdateAssociate_AssociateNotFound_ExceptionThrown() {
		Mockito.when(associateRepository.findById(associateDto.getId())).thenReturn(Optional.empty());
		Assertions.assertThrows(AssociateException.class, () -> associateService.update(associateDto));
		Mockito.verify(associateRepository, Mockito.times(1)).findById(associateDto.getId());
		Mockito.verify(associateRepository, Mockito.never()).save(Mockito.any());
	}

	@Test
	void testGetByGender_InvalidGender_ExceptionThrown() {
		String gender = "X";
		Assertions.assertThrows(AssociateException.class, () -> associateService.getByGender(gender));
		Mockito.verify(associateRepository, Mockito.never()).findByGender(Mockito.anyString());
		Mockito.verify(modelMapper, Mockito.never()).map(Mockito.any(), Mockito.eq(AssociateDto.class));
	}

	@Test
	void testGetByGender_ValidGender_ReturnsAssociateDtoList() {
		String gender = "M";
		List<Associate> associateList = new ArrayList<>();
		associateList.add(associate);
		List<AssociateDto> expectedDtoList = new ArrayList<>();
		expectedDtoList.add(associateDto);
		Mockito.when(associateRepository.findByGender(gender)).thenReturn(associateList);
		Mockito.when(modelMapper.map(associate, AssociateDto.class)).thenReturn(associateDto);
		List<AssociateDto> result = associateService.getByGender(gender);
		Assertions.assertEquals(expectedDtoList.size(), result.size());
		Assertions.assertEquals(expectedDtoList, result);
		Mockito.verify(associateRepository, Mockito.times(1)).findByGender(gender);
		Mockito.verify(modelMapper, Mockito.times(1)).map(associate, AssociateDto.class);
		Mockito.verify(modelMapper, Mockito.times(1)).map(associate, AssociateDto.class);
	}
	
}
