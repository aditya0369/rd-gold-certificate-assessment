package com.epam.batchapp;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.epam.batchapp.controller.AssociateRestController;
import com.epam.batchapp.dto.AssociateDto;
import com.epam.batchapp.exception.AssociateException;
import com.epam.batchapp.model.Batch;
import com.epam.batchapp.service.AssociateService;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest(AssociateRestController.class)
class ControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private AssociateService associateService;

	@MockBean
	private ModelMapper modelMapper;

	private final ObjectMapper objectMapper = new ObjectMapper();

	@Test
	void testAdd_ValidAssociate_ReturnsCreated() throws Exception {
		Batch batch = Batch.builder().id(1).name("Demo").build();
		AssociateDto associateDto = new AssociateDto(1, "Adi", "adi@gm.com", "M", "Chitkara", "ACTIVE", batch);
		Mockito.when(associateService.update(Mockito.any(AssociateDto.class))).thenReturn(associateDto);

		Mockito.when(associateService.add(Mockito.any(AssociateDto.class))).thenReturn(associateDto);

		mockMvc.perform(post("/rd/associates").contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(associateDto))).andExpect(status().isCreated())
				.andExpect(jsonPath("$.id").value(1)).andExpect(jsonPath("$.name").value("Adi"))
				.andExpect(jsonPath("$.gender").value("M"));
	}

	@Test
	void testUpdate_ValidAssociate_ReturnsOk() throws Exception {
		Batch batch = Batch.builder().id(1).name("Demo").build();
		AssociateDto associateDto = new AssociateDto(1, "Adi", "adi@gm.com", "M", "Chitkara", "ACTIVE", batch);
		Mockito.when(associateService.update(Mockito.any(AssociateDto.class))).thenReturn(associateDto);

		mockMvc.perform(put("/rd/associates").contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(associateDto))).andExpect(status().isOk())
				.andExpect(jsonPath("$.id").value(1)).andExpect(jsonPath("$.name").value("Adi"))
				.andExpect(jsonPath("$.gender").value("M"));
	}

	@Test
	void testDeleteAssociate_ValidId_ReturnsNoContent() throws Exception {
		mockMvc.perform(delete("/rd/associates/{id}", 1)).andExpect(status().isNoContent());
	}

	@Test
	void testGetByGender_ValidGender_ReturnsAssociates() throws Exception {
		AssociateDto associateDto1 = AssociateDto.builder().id(1).name("John Doe").gender("M").build();
		AssociateDto associateDto2 = AssociateDto.builder().id(2).name("Jane Smith").gender("F").build();
		List<AssociateDto> associateDtoList = Arrays.asList(associateDto1, associateDto2);

		Mockito.when(associateService.getByGender(Mockito.anyString())).thenReturn(associateDtoList);

		mockMvc.perform(get("/rd/associates/{gender}", "M")).andExpect(status().isOk())
				.andExpect(jsonPath("$[0].id").value(1)).andExpect(jsonPath("$[0].name").value("John Doe"))
				.andExpect(jsonPath("$[0].gender").value("M")).andExpect(jsonPath("$[1].id").value(2))
				.andExpect(jsonPath("$[1].name").value("Jane Smith")).andExpect(jsonPath("$[1].gender").value("F"));
	}

	@Test
	void testGetAssociateByGenderThrowsDataIntegrityViolationException() throws Exception {
		Batch batch = Batch.builder().id(1).name("Demo").build();
		AssociateDto associateDto = new AssociateDto(1, "Adi", "adi@gm.com", "M", "Chitkara", "ACTIVE", batch);
		when(associateService.getByGender("M"))
				.thenThrow(new DataIntegrityViolationException("Data Integrity exception"));
		mockMvc.perform(get("/rd/associates/{gender}", "M").contentType(MediaType.APPLICATION_JSON)
				.content(new ObjectMapper().writeValueAsString(associateDto))).andExpect(status().isBadRequest());
	}

	@Test
	void testGetAssociateByGenderThrowsRuntimeException() throws Exception {
		Batch batch = Batch.builder().id(1).name("Demo").build();
		AssociateDto associateDto = new AssociateDto(1, "Adi", "adi@gm.com", "M", "Chitkara", "ACTIVE", batch);
		when(associateService.getByGender("M")).thenThrow(new RuntimeException("Runtime exception"));
		mockMvc.perform(get("/rd/associates/{gender}", "M").contentType(MediaType.APPLICATION_JSON)
				.content(new ObjectMapper().writeValueAsString(associateDto))).andExpect(status().isBadRequest());
	}

	@Test
	void testGetAssociateByGenderThrowsAssociateException() throws Exception {
		Batch batch = Batch.builder().id(1).name("Demo").build();
		AssociateDto associateDto = new AssociateDto(1, "Adi", "adi@gm.com", "M", "Chitkara", "ACTIVE", batch);
		when(associateService.getByGender("M")).thenThrow(new AssociateException("Associate exception"));
		mockMvc.perform(get("/rd/associates/{gender}", "M").contentType(MediaType.APPLICATION_JSON)
				.content(new ObjectMapper().writeValueAsString(associateDto))).andExpect(status().isBadRequest());
	}
}